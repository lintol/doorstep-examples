import json
import gettext
from dask.threaded import get
import re
import unidecode
import chardet
import sys
import re
import logging
from jsonpath_ng import jsonpath, parse as jsonpath_parse

from ltldoorstep.processor import DoorstepProcessor
# from ltldoorstep.aspect import AnnotatedTextAspect
from ltldoorstep.reports.report import combine_reports

gettext.install('ltldoorstep')

def note_counts(tree, rprt):
    find_path = jsonpath_parse('data.[*].newCasesByPublishDate.`parent`')
    matches = find_path.find(tree)
    for match in matches:
        date = match.value['date']
        count = match.value['newCasesByPublishDate']
        field = match.full_path.child('newCasesByPublishDate')
        rprt.add_issue(
            logging.WARNING,
            'count-entry',
            _("Found a count for {}").format(date),
            json_path=field,
            content=count,
            tree=tree
        )

    return rprt

def set_properties(df, rprt):
    return rprt

def load_json(filename):
    with open(filename, 'r') as f:
        data = json.load(f)
    return data

class CovidTrackerTreeProcessor(DoorstepProcessor):
    preset = 'tree'
    code = 'lintol-covid-tracker-tree:1'
    description = _("Covid Tracker Processor (Tree)")

    def initialize(self, report=None, context=None):
        return super().initialize(report, context)

    def get_workflow(self, filename, metadata={}):
        workflow = {
            'load-json': (load_json, filename),
            'report': (set_properties, 'load-json', self.make_report()),
            'step-A': (note_counts, 'load-json', 'report'),
            'output': (workflow_condense, 'step-A')
        }
        return workflow

def workflow_condense(base, *args):
    return combine_reports(*args, base=base)

processor = CovidTrackerTreeProcessor.make

if __name__ == "__main__":
    argv = sys.argv
    processor = CovidTrackerTreeProcessor()
    processor.initialize()
    workflow = processor.build_workflow(argv[1])
    print(get(workflow, 'output'))
