import json
import gettext
from dask.threaded import get
import re
import unidecode
import chardet
import sys
import re
import logging
import spacy
from spellchecker import SpellChecker
from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.aspect import AnnotatedTextAspect
from ltldoorstep.reports.report import combine_reports
from ltldoorstep.document_utils import load_text, split_into_paragraphs

gettext.install('ltldoorstep')

PARAGRAPH_LIMIT = 10000
def spellcheck(text, rprt, nlp):
    checker = SpellChecker()
    proper_nouns = {}
    # Assuming a direct double newline for paragraph
    line_counter = 0
    paragraphs = []
    counter = 0
    text = unidecode.unidecode(text).replace("'", '')
    paragraphs = split_into_paragraphs(text)

    # All characters are Latinate (unicodedata.normalize('NFKD')[0] is Latin)
    docs = nlp.pipe([para[0] for para in paragraphs])
    for para_n, (doc, (paragraph, line_number)) in enumerate(zip(docs, paragraphs)):
        if re.match('^[^a-zA-Z]*$', paragraph): # Ignore paragraphs with no characters
            continue

        to_check = [tok for tok in doc if tok.pos_ != 'PROPN' and len(tok) > 2]

        proper_nouns_in_para = [tok for tok in doc if tok.pos_ == 'PROPN' and len(tok) > 2]
        proper_nouns.update({str(tok).lower(): str(tok) for tok in proper_nouns_in_para})

        failed = checker.unknown(map(str, to_check))
        to_note = [(tok, checker.candidates(str(tok))) for tok in doc if str(tok) in failed]

        if to_note or proper_nouns_in_para:
            content = AnnotatedTextAspect(doc)
            issue_text = []
            for tok, suggestions in to_note:
                content.add(
                    note=', '.join(suggestions),
                    start_offset=tok.idx,
                    end_offset=tok.idx + len(tok),
                    level=logging.WARNING,
                    tags=['spelling']
                )
                issue_text.append(f'"{tok}"')

            for tok in proper_nouns_in_para:
                content.add(
                    note='Proper noun found',
                    start_offset=tok.idx,
                    end_offset=tok.idx + len(tok),
                    level=logging.INFO,
                    tags=['proper-noun']
                )

            snippet = ''
            if para_n > 0:
                snippet += paragraphs[para_n - 1][0]
            snippet += paragraph
            if para_n < len(paragraphs) - 1:
                snippet += paragraphs[para_n + 1][0]

            if to_note:
                rprt.add_issue(
                    logging.WARNING,
                    'spelling-suggestions',
                    _("Spelling issues in paragraph {n}: ").format(n=para_n) + ', '.join(issue_text),
                    line_number=line_number,
                    character_number=0,
                    snippet=snippet,
                    content=content
                )

            if proper_nouns_in_para:
                proper_nouns_in_para = {str(tok) for tok in proper_nouns_in_para}
                rprt.add_issue(
                    logging.INFO,
                    'proper-noun-highlight',
                    _("Proper nouns found in paragraph {n}: ").format(n=para_n) + ', '.join(proper_nouns_in_para),
                    line_number=line_number,
                    character_number=0,
                    snippet=snippet,
                    content=content
                )
    rprt.add_issue(
        logging.INFO,
        'proper-noun-summary',
        _("Proper nouns found overall: {n} ").format(n=len(proper_nouns)) + ', '.join(sorted(proper_nouns.values())),
    )

    return rprt

def set_properties(df, rprt):
    return rprt

class SpellCheckerProcessor(DoorstepProcessor):
    preset = 'document'
    code = 'lintol-spell-checker-document:1'
    description = _("Spellcheck Processor (Document)")

    _spacy_model = 'en_core_web_sm'

    def initialize(self, report=None, context=None):
        self.nlp = spacy.load(self._spacy_model)
        return super().initialize(report, context)

    def get_workflow(self, filename, metadata={}):
        workflow = {
            'load-txt': (load_text, filename),
            'report': (set_properties, 'load-txt', self.make_report()),
            'step-A': (spellcheck, 'load-txt', 'report', self.nlp),
            'output': (workflow_condense, 'step-A')
        }
        return workflow

def workflow_condense(base, *args):
    return combine_reports(*args, base=base)

processor = SpellCheckerProcessor.make

if __name__ == "__main__":
    argv = sys.argv
    processor = SpellCheckerProcessor()
    processor.initialize()
    workflow = processor.build_workflow(argv[1])
    print(get(workflow, 'output'))
