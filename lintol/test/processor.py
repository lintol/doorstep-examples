"""
Lintol Test Processor
(c) Lintol Ltd, 2020 - MIT LICENSE

Copyright <YEAR> <COPYRIGHT HOLDER>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import sys
import pandas as pd
import chardet
import gettext
import logging
from dask.threaded import get
from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.reports.report import combine_reports

gettext.install('ltldoorstep')


def set_properties(df, rprt):
    rprt.set_properties(headers=list(df.columns))
    return rprt

def step_a(csv, rprt):
    return rprt

def step_b(df, rprt):
    return rprt

def step_c(csv, rprt):
    return rprt

def step_d(csv, rprt):
    return rprt


def read_csv(filename):
    with open(filename, 'rb') as f:
        result = chardet.detect(f.read(1024))

    retry = False
    while True:
        try:
            data = pd.read_csv(
                filename,
                encoding=result['encoding'],
                low_memory=False,
                comment='#',
                na_values=('*',),
                keep_default_na=True
            )
            break
        except UnicodeDecodeError:
            if retry:
                break
            # If we didn't get the encoding right the first
            # time, try the whole file
            with open(filename, 'rb') as f:
                result = chardet.detect(f.read())
            retry = True

    return data

class LintolTestProcessor(DoorstepProcessor):
    preset = 'tabular'
    code = 'lintol-test:1'
    description = _("Lintol Test Processor")

    def compile_report(self, filename=None, metadata=None):
        print(self._report)
        output = self._report.compile(filename, metadata)
        print(output)
        return output

    def get_workflow(self, filename, metadata={}):
        workflow = {
            'load-csv': (read_csv, filename),
            'step-A': (step_a, 'load-csv', self.make_report()),
            'step-B': (step_b, 'load-csv', self.make_report()),
            'step-C': (step_c, 'load-csv', self.make_report()),
            'step-D': (step_d, 'load-csv', self.make_report()),
            'condense': (workflow_condense, 'step-A', 'step-B', 'step-B', 'step-D'),
            'output': (set_properties, 'load-csv', 'condense')
        }
        return workflow

def workflow_condense(base, *args):
    return combine_reports(*args, base=base)

def run(filename):
    """
    Run in CLI environment, without framework, for experimentation
    """

    workflow = processor().build_workflow(filename)
    print(get(workflow, 'output'))

processor = LintolTestProcessor

if __name__ == "__main__":
    run(sys.argv[1])
