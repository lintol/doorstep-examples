require 'csvlint'
require 'net/http'
require 'json'

class Handler
  def run(req)
    args = JSON.parse(req)
    metadata = JSON.parse(args["metadata"])
    filename = args["filename"]
    input_file = "/tmp/file.csv"

    File.open(input_file, "wb") do |f|
      uri = URI.parse(filename)
      Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
        http.use_ssl = true
        http.request_get(uri.path) do |rq|
          rq.read_body do |segment|
            f << segment
          end
        end
      end
    end

    dialect = {
      "header" => "true",
      "delimiter" => ","
    }

    if metadata.key?("configuration") && metadata["configuration"].key?("delimiter")
      if metadata["configuration"]["delimiter"] == "tab"
        dialect["delimiter"] = "\t"
      end
    end

    validator = Csvlint::Validator.new(input_file, dialect)

    translations = {
        :wrong_content_type => "Content type is not text/csv",
        :ragged_rows => "Row has a different number of columns (than the first row in the file)",
        :blank_rows => "Completely empty row, e.g. blank line or a line where all column values are empty",
        :invalid_encoding => "Encoding error when parsing row, e.g. because of invalid characters",
        :not_found => "HTTP 404 error when retrieving the data",
        :stray_quote => "Missing or stray quote",
        :unclosed_quote => "Unclosed quoted field",
        :whitespace => "A quoted column has leading or trailing whitespace",
        :line_breaks => "Line breaks were inconsistent or incorrectly specified"
    }

    if validator.errors
      errors = validator.errors.map { |error|
        if error.row
          row = validator.data[error.row - 1]
        else
          row = nil
        end
        {
           "processor" => "theodi/csvlint.rb:1",
           "message" => "Row #{error.row}, #{error.column}: #{translations[error.type]}",
           "row" => row,
           "row-number" => error.row,
           "code" => error.type,
           "column-number" => error.column,
           "item" => {
             "entity" => {
               "location" => {
                 "row" => error.row,
                 "column" => error.column
                },
               "definition": {},
               "type": "cell"
             },
             "properties" => {}
           },
           "context" => [
             {
               "entity" => {
                 "type" => "row",
                 "location" => {
                   "row" => error.row
                  },
                 "definition": row
               },
               "properties" => {}
             }
           ]
        }
      }
    end

    report = {
      "error-count" => errors.length(),
      "valid" => errors.empty?,
      "row-count" => validator.row_count,
      "headers" => validator.data[0],
      "filename" => input_file,
      "supplementary" => [],
      "preset" => "tabular",
      "time" => 0.0,
      "tables" => [
         {
            "headers" => validator.data[0],
            "format" => validator.extension,
            "row-count" => validator.row_count,
            "errors" => errors,
            "warnings" => [],
            "informations" => [],
            "table-count" => 1,
            "time" => 0.0,
            "valid" => errors.empty?,
            "scheme" => "file",
            "encoding" => validator.encoding,
            "schema" => nil,
            "error-count" => errors.length()
         }
      ]
    }

    return JSON.pretty_generate(report)
  end
end
