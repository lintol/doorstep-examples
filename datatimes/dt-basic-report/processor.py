"""This script will attempt to classify any files by semantic location

"""

import json
import time
import gettext
import pandas as p
import logging
import sys
import os
import ltldoorstep
import gettext
from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.reports import report
from dask.threaded import get
import requests

import json

def classify(rprt, context):
    tags = context.package['tags']
    categories = []
    for tag in tags:
        categories.append(tag['name'])

    issue_text = _("Additional categories: {}").format(", ".join(categories))

    rprt.add_issue(
        logging.INFO,
        'additional-categories',
        issue_text,
        row_number='Metadata',
        error_data=categories,
        at_top=True
    )

    return rprt


class DTBasicReportProcessor(DoorstepProcessor):
    @staticmethod
    def make_report():
        return report.TabularReport(
            'datatimes/dt-basic-report:1',
            _("Data Times Basic Report Processor")
        )

    def get_workflow(self, filename, context):
        # setting up workflow dict
        workflow = {
            'output': (classify, self._report, context)
        }
        return workflow

processor = DTBasicReportProcessor.make

def requires(metadata):
    return {'download': False}

if __name__ == '__main__':
    gettext.install('ltldoorstep')
    proc = processor()

    metadata = {}
    if len(sys.argv) > 2:
        with open(sys.argv[2], 'r') as metadata_f:
            metadata = json.load(metadata_f)

    workflow = proc.build_workflow(sys.argv[1], metadata)
    print(get(workflow, 'output'))
