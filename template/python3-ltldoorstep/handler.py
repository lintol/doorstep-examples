from flask import current_app

try:
    from minio import Minio
    from minio.error import S3Error as MinioResponseError
except ImportError:
    Minio = None
    MinioResponseError = RuntimeError

import asyncio
import os
import io
import errno

import logging
from dask import threaded
from ltldoorstep.config import set_config, load_config
from ltldoorstep.location_utils import load_berlin
import shutil
import json
import tempfile
import requests
from flask_restful import Resource, abort, reqparse
from ltldoorstep.context import DoorstepContext
from ltldoorstep.config import load_config
from ltldoorstep.encoders import json_dumps
from ltldoorstep.errors import LintolDoorstepException
from ltldoorstep.reports.report import Report
import function.processor as mod
import os


class Handler(Resource):
    _bucket = None
    _config = {}
    _mo = None

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('filename', location='json')
        parser.add_argument('context', location='json')
        parser.add_argument('target', location='json')
        args = parser.parse_args()
        context = args['context']
        filename = args['filename']
        target = args['target']

        context = DoorstepContext.from_dict(json.loads(context))

        requested_artifacts = []
        if 'artifacts' in context.settings and type(context.settings['artifacts']) is list:
            requested_artifacts += context.settings['artifacts']

        if 'artifactsRequested' in context.configuration and type(context.configuration['artifactsRequested']) is dict:
            requested_artifacts += [k for k, v in context.configuration['artifactsRequested'].items() if v]

        requested_artifacts = {artifact: '{}/artifact_{}'.format(target, n) for n, artifact in enumerate(requested_artifacts)}

        with tempfile.TemporaryDirectory() as tempdirname:
            input_filename = os.path.join(tempdirname, 'input_file')
            with open(input_filename, 'w+b') as f:
                try:
                    processor = mod.processor()
                    processor.set_artifacts_to_be_requested(list(requested_artifacts.keys()))
                except Exception as e:
                    if not isinstance(e, LintolDoorstepException):
                        e = LintolDoorstepException(e)
                    current_app.logger.error(e)
                    return {'error': 5, 'exception': json.loads(json_dumps(e))}

                if 'metadataOnly' in context.configuration and context.configuration['metadataOnly']:
                    filename = None
                    current_app.logger.error("Metadata Only request.")
                else:
                    current_app.logger.error("Full request.")
                    if filename.startswith('https:') or filename.startswith('http:'):
                        r = requests.get(filename)
                        if r.status_code == 200:
                            for chunk in r.iter_content(chunk_size=1024):
                                f.write(chunk)
                    else:
                         message = _(f'Filename is not URL: {filename}.')
                         current_app.logger.error(message)
                         e = FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)
                         e = LintolDoorstepException(
                             e,
                             processor=processor.code,
                             message=message
                         )
                         return {'error': 2, 'exception': json.loads(json_dumps(e))}

                    f.flush()
                    filename = f.name

                try:
                    workflow = processor.build_workflow(filename, context)
                    result = threaded.get(workflow, 'output', num_workers=2)
                except Exception as e:
                    if not isinstance(e, LintolDoorstepException):
                        e = LintolDoorstepException(e)
                    current_app.logger.error(e)
                    return {'error': 1, 'exception': json.loads(json_dumps(e))}

                try:
                    if isinstance(result, tuple) and len(result) == 2 and isinstance(result[1], Report):
                        processor.set_report(result[1])
                    elif isinstance(result, Report):
                        processor.set_report(result)

                    for artifact, artifact_target in requested_artifacts.items():
                        artifact_type = processor.get_artifact_type(artifact)
                        processor.get_report().record_artifact(artifact, artifact_target, artifact_type)
                except Exception as e:
                    if not isinstance(e, LintolDoorstepException):
                        e = LintolDoorstepException(e)
                    current_app.logger.error(e)
                    return {'error': 2, 'exception': json.loads(json_dumps(e))}

                try:
                    result = processor.compile_report(filename, context)
                except Exception as e:
                    if not isinstance(e, LintolDoorstepException):
                        e = LintolDoorstepException(e)
                    current_app.logger.error(e)
                    return {'error': 3, 'exception': json.loads(json_dumps(e))}

                session = {
                    'result': result,
                    'completion': asyncio.Event()
                }
                session['completion'].set()

                try:
                    for n, (artifact, artifact_target) in enumerate(requested_artifacts.items()):
                        typ = processor.get_artifact_type(artifact)

                        stream_req = io.BytesIO()
                        if typ.is_bytes:
                            output_req = stream_req
                        else:
                            output_req = io.TextIOWrapper(stream_req, encoding=typ.encoding)

                        processor.get_artifact(artifact, output_req)

                        output_req.flush()

                        pos = stream_req.tell()
                        stream_req.seek(0, os.SEEK_END)
                        length = stream_req.tell()
                        stream_req.seek(0)

                        self.write_to_minio(stream_req, artifact_target, length)

                    result = json.loads(json.dumps(result), parse_constant=lambda x: None)
                except Exception as e:
                    if not isinstance(e, LintolDoorstepException):
                        e = LintolDoorstepException(e)
                    current_app.logger.error(e)
                    return {'error': 4, 'exception': json.loads(json_dumps(e))}

        return result

    @classmethod
    def preload(cls):
        cls._config = load_config()

        set_config('reference-data.storage', 'minio')
        set_config('storage.minio.region', 'us-east-1') # Fixed by Minio
        for k in ('bucket', 'key', 'secret', 'prefix', 'endpoint'):
            filename = os.path.join('/var', 'openfaas', 'secrets', f'minio_{k}')
            with open(filename, 'r') as f:
                value = f.read().strip()

            if k == 'prefix':
                set_config('reference-data.prefix', value)
            else:
                set_config(f'storage.minio.{k}', value)

        # e.g. load_berlin()
        if hasattr(mod, 'preload'):
            mod.preload()

        debug = cls._config['debug'] if 'debug' in cls._config else False
        logging.basicConfig(level=logging.DEBUG if debug else logging.INFO)
        cls.logger = logging.getLogger(__name__)

        return True

    def write_to_minio(self, stream_req, target, length):
        config = self._config

        if self._mo is None:
            self._mo = Minio(
            config['storage']['minio']['endpoint'],
            access_key=config['storage']['minio']['key'],
            secret_key=config['storage']['minio']['secret'],
            region=config['storage']['minio']['region'],
            secure=True
        )
        mo_bucket = config['storage']['minio']['bucket']

        # stream_req = io.BytesIO()
        # result_string = json.dumps(result).encode('utf-8')
        # stream_req.write(result_string)
        try:
                self.logger.error(str([mo_bucket, target, length]))
                data_object = self._mo.put_object(mo_bucket, target, stream_req, length)
        except MinioResponseError as err:
            raise err
